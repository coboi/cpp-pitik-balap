namespace lv {
	struct Level{
		std::string id;
		std::string name;
		int power;
		int level;
	};

	// kabupaten
	Level Satu;
	Level Dua;
	Level Tiga;
	Level Empat;
	Level Lima;
	// provinsi
	Level Enam;
	Level Tujuh;
	Level Delapan;
	Level Sembilan;
	Level Sepuluh;
	Level Sebelas;
	Level Duabelas;
	// nasional
	Level Tigabelas;
	Level Empatbelas;
	Level Limabelas;
	Level Enambelas;
	Level Tujuhbelas;
	Level Delapanbelas;
	Level Sembilanbelas;
	Level Duapuluh;
	Level Duapuluhsatu;
	Level Duapuluhdua;

	void LevelData(){
		Satu.id = "A002";
		Satu.name = "Anarap";
		Satu.power = 132;
		Satu.level = 101;

		Dua.id = "A003";
		Dua.name = "Haban";
		Dua.power = 187;
		Dua.level = 102;

		Tiga.id = "A004";
		Tiga.name = "Leoa";
		Tiga.power = 265;
		Tiga.level = 103;

		Empat.id = "A005";
		Empat.name = "Heandd";
		Empat.power = 352;
		Empat.level = 104;

		Lima.id = "A006";
		Lima.name = "Elovar";
		Lima.power = 435;
		Lima.level = 105;

		// provinsi
		Enam.id = "A007";
		Enam.name = "Arrrgg";
		Enam.power = 502;
		Enam.level = 106;

		Tujuh.id = "A008";
		Tujuh.name = "Herdeu";
		Tujuh.power = 590;
		Tujuh.level = 107;

		Delapan.id = "A009";
		Delapan.name = "Keliv";
		Delapan.power = 650;
		Delapan.level = 108;

		Sembilan.id = "A010";
		Sembilan.name = "Manau";
		Sembilan.power = 762;
		Sembilan.level = 109;

		Sepuluh.id = "A011";
		Sepuluh.name = "Rebq";
		Sepuluh.power = 819;
		Sepuluh.level = 110;

		Sebelas.id = "A012";
		Sebelas.name = "Dssa";
		Sebelas.power = 921;
		Sebelas.level = 111;

		Duabelas.id = "A013";
		Duabelas.name = "Rirx";
		Duabelas.power = 1004;
		Duabelas.level = 112;

		// nasional
		Tigabelas.id = "A014";
		Tigabelas.name = "Kamrt";
		Tigabelas.power = 1057;
		Tigabelas.level = 113;

		Empatbelas.id = "A015";
		Empatbelas.name = "Llotu";
		Empatbelas.power = 1139;
		Empatbelas.level = 114;

		Limabelas.id = "A016";
		Limabelas.name = "Rozh";
		Limabelas.power = 1208;
		Limabelas.level = 115;

		Enambelas.id = "A017";
		Enambelas.name = "Gisli";
		Enambelas.power = 1302;
		Enambelas.level = 116;

		Tujuhbelas.id = "A018";
		Tujuhbelas.name = "Sokc";
		Tujuhbelas.power = 1393;
		Tujuhbelas.level = 117;

		Delapanbelas.id = "A019";
		Delapanbelas.name = "Esidi";
		Delapanbelas.power = 1457;
		Delapanbelas.level = 118;

		Sembilanbelas.id = "A020";
		Sembilanbelas.name = "Jori";
		Sembilanbelas.power = 1539;
		Sembilanbelas.level = 119;

		Duapuluh.id = "A021";
		Duapuluh.name = "Omem";
		Duapuluh.power = 1616;
		Duapuluh.level = 120;

		Duapuluhsatu.id = "A022";
		Duapuluhsatu.name = "Wavo";
		Duapuluhsatu.power = 1695;
		Duapuluhsatu.level = 121;

		Duapuluhdua.id = "A023";
		Duapuluhdua.name = "JonathanJoestar";
		Duapuluhdua.power = 1889;
		Duapuluhdua.level = 122;
	}
}