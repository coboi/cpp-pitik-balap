#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include "levels.hpp"
#include "ayam.hpp"

std::string cmd;
int gachaUpgrade;
int gachaRate;

int main(){
	ayam::dataKu.open("data.dat", std::ios::in);
	ayam::dataKu >> ayam::isSaved;
	ayam::dataKu.close();

	if (ayam::isSaved == "y"){
		ayam::InputData();
	} else{
		std::cout << "Haiiii, selamat datang ^^" << std::endl;
		std::cout << "Ketik daftar_perintah untuk lihat perintahnya" << std::endl;
		ayam::setAyamKu();
	}

	lv::LevelData();
	ayam::SelectLevel();

	while(true){
		std::cout << "/";
		std::cin >> cmd;

		if (cmd == "ayam"){
			std::cout << "ID ayam: " << ayam::AyamKu.getId() << std::endl;
			std::cout << "Nama ayam: " << ayam::AyamKu.getName() << std::endl;
			std::cout << "Power ayam: " << ayam::AyamKu.getPower() << std::endl;

			std::cout << "Prestasi: " << std::endl;
			if ((ayam::AyamMu.getLevel() >= 101) && (ayam::AyamMu.getLevel() <= 105)){
				std::cout << "  -" << std::endl;
			} else if ((ayam::AyamMu.getLevel() >= 106) && (ayam::AyamMu.getLevel() <= 112)){
				std::cout << "  Juara kabupaten" << std::endl;
			} else if ((ayam::AyamMu.getLevel() >= 113) && (ayam::AyamMu.getLevel() <= 122)){
				std::cout << "  Juara kabupaten" << std::endl;
				std::cout << "  Juara provinsi" << std::endl;
			} else if (ayam::AyamMu.getLevel() >= 123){
				std::cout << "  Juara kabupaten" << std::endl;
				std::cout << "  Juara provinsi" << std::endl;
				std::cout << "  Juara nasional" << std::endl;
			}

			std::cout << "> Perintah: ayam_nama, ayam_upgrade" << std::endl;
		} else if (cmd == "ayam_nama"){
			std::cout << "Nama ayam: " << ayam::AyamKu.getName() << std::endl;
			std::cout << "Masukkan nama baru: ";
			std::cin >> ayam::tempName;
			ayam::AyamKu.setName(ayam::tempName);
			std::cout << "Nama ayam sekarang: " << ayam::AyamKu.getName() << std::endl;
			ayam::SaveData();
		} else if (cmd == "ayam_upgrade"){
			std::srand(time(NULL));
			gachaUpgrade = (std::rand() % 50) + 20;
			gachaRate = (std::rand() % 100) + 1;
			std::cout << "Power upgrade: " << gachaUpgrade << std::endl;
			std::cout << "Kemungkinan berhasil: 50%" << std::endl;
			std::cout << "Jika gagal, powermu akan dikurangin " << gachaUpgrade << std::endl;
			std::cout << "> Perintah: ayam_upgrade_y" << std::endl;
		} else if (cmd == "ayam_upgrade_y"){
			if (gachaUpgrade == 0){
				std::cout << "Ke ayam_upgrade dulu" << std::endl;
			} else{
				if ((gachaRate >= 1) && (gachaRate <= 50)){
					std::cout << "Upgrade gagal" << std::endl;
					std::cout << "Power lama: " << ayam::AyamKu.getPower() << std::endl;
					ayam::AyamKu.setPower(ayam::AyamKu.getPower() - gachaUpgrade);
					ayam::SaveData();
					std::cout << "Power sekarang: " << ayam::AyamKu.getPower() << std::endl;
					gachaUpgrade = 0;
				} else if ((gachaRate >= 51) && (gachaRate <= 100)){
					std::cout << "Upgrade berhasil yuhuu" << std::endl;
					std::cout << "Power lama: " << ayam::AyamKu.getPower() << std::endl;
					ayam::AyamKu.setPower(ayam::AyamKu.getPower() + gachaUpgrade);
					ayam::SaveData();
					std::cout << "Power sekarang: " << ayam::AyamKu.getPower() << std::endl;
					gachaUpgrade = 0;
				} else {
					std::cout << "Error: upgrade" << std::endl;
					gachaUpgrade = 0;
				}
			}
		} else if (cmd == "balap"){
			if ((ayam::AyamMu.getLevel() >= 101) && (ayam::AyamMu.getLevel() <= 105)){
				std::cout << "Tingkat: Kabupaten" << std::endl;
			} else if ((ayam::AyamMu.getLevel() >= 106) && (ayam::AyamMu.getLevel() <= 112)){
				std::cout << "Tingkat: Provinsi" << std::endl;
			} else if ((ayam::AyamMu.getLevel() >= 113) && (ayam::AyamMu.getLevel() <= 122)){
				std::cout << "Tingkat: Nasional" << std::endl;
			}

			if (ayam::currentLevel == 123){
				std::cout << "Kamu juara nasional, dah gak ada musuh :)" << std::endl;
			} else{
				std::cout << "Level: " << ayam::AyamMu.getLevel() << std::endl;
				std::cout << "Musuh: " << ayam::AyamMu.getName() << std::endl;
				std::cout << "> Perintah: balap_gas" << std::endl;
			}
		} else if (cmd == "balap_gas"){
			if ((ayam::currentLevel == ayam::AyamMu.getLevel()) && (ayam::currentLevel <= 122)){
				std::cout << "Level: " << ayam::AyamMu.getLevel() << std::endl;
				std::cout << "Powermu: " << ayam::AyamKu.getPower() << std::endl;
				std::cout << "Power musuh: " << ayam::AyamMu.getPower() << std::endl;

				if (ayam::AyamKu.getPower() > ayam::AyamMu.getPower()){
					std::cout << "Yayy, kamu menang dari " << ayam::AyamMu.getName() << std::endl;
					ayam::currentLevel += 1;
					ayam::SelectLevel();
					ayam::SaveData();

					if (ayam::currentLevel == 106){
						std::cout << "Yyyyyyyaaayyyy, kamu juara kabupaten ^^" << std::endl;
					} else if (ayam::currentLevel == 113){
						std::cout << "Yyyyyyyaaayyyy, kamu juara provinsi ^^" << std::endl;
					} else if (ayam::currentLevel == 123){
						std::cout << "Yyyyyyyaaayyyy, kamu juara nasional ^^" << std::endl;
					}
				} else if (ayam::AyamKu.getPower() < ayam::AyamMu.getPower()){
					std::cout << "Yahhh, kamu kalah :(" << std::endl;
				} else{
					std::cout << "Imbang, coba lagi nanti" << std::endl;
				}
			} else if (ayam::currentLevel == 123){
				std::cout << "Kamu juara nasional, dah gak ada musuh :)" << std::endl;
			} else{
				std::cout << "Level tidak cocok" << std::endl;
			}
		} else if (cmd == "daftar_perintah"){
			std::cout << "Ayam" << std::endl;
			std::cout << "  ayam - lihat info ayam" << std::endl;
			std::cout << "  ayam_nama - ganti nama ayam" << std::endl;
			std::cout << "  ayam_upgrade - upgrade power ayam" << std::endl;
			std::cout << "  ayam_upgrade_y - konfirmasi upgrade" << std::endl;
			std::cout << "Balap" << std::endl;
			std::cout << "  balap - lihat info balap" << std::endl;
			std::cout << "  balap_gas - konfirmasi balap" << std::endl;
			std::cout << "Daftar perintah" << std::endl;
			std::cout << "  daftar_perintah - lihat daftar perintah" << std::endl;
			std::cout << "Tentang" << std::endl;
			std::cout << "  tentang - tentang game ini" << std::endl;
			std::cout << "Keluar" << std::endl;
			std::cout << "  keluar - keluar dari game" << std::endl;
		} else if (cmd == "tentang"){
			std::cout << "pitik balap - 2019" << std::endl;
			std::cout << "coboi c0boi c0b0i" << std::endl;
		} else if (cmd == "keluar"){
			ayam::SaveData();
			break;
		} else{
			std::cout << "Perintah salah" << std::endl;
		}
	}
	// Hei :)
	return 0;
}