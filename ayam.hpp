namespace ayam {
	class Ayam{
	protected:
		std::string id_;
		std::string name_;
		int power_;
	public:
		virtual std::string getId() = 0;
		virtual std::string getName() = 0;
		virtual int getPower() = 0;
		virtual void setId(std::string id) = 0;
		virtual void setName(std::string name) = 0;
		virtual void setPower(int power) = 0;
	};

	class AyamPlayer:public Ayam{
	public:
		std::string getId(){
			return id_;
		}
		std::string getName(){
			return name_;
		}
		int getPower(){
			return power_;
		}
		void setId(std::string id){
			id_ = id;
		}
		void setName(std::string name){
			name_ = name;
		}
		void setPower(int power){
			power_ = power;
		}
	};

	class AyamMusuh:public Ayam{
	protected:
		int level_;
	public:
		std::string getId(){
			return id_;
		}
		std::string getName(){
			return name_;
		}
		int getPower(){
			return power_;
		}
		int getLevel(){
			return level_;
		}
		void setId(std::string id){
			id_ = id;
		}
		void setName(std::string name){
			name_ = name;
		}
		void setPower(int power){
			power_ = power;
		}
		void setLevel(int level){
			level_ = level;
		}
	};

	AyamPlayer AyamKu;
	AyamMusuh AyamMu;
	std::fstream dataKu;
	std::string isSaved;
	std::string tempId;
	std::string tempName;
	int tempPower;
	int currentLevel = 101;

	void setAyamKu(){
		AyamKu.setId("A001");
		AyamKu.setName("Ayayam");
		AyamKu.setPower(140);
	}

	void SaveData(){
		dataKu.open("data.dat", std::ios::out);
		dataKu << "y" << std::endl;
		dataKu << "A001" << std::endl;
		dataKu << AyamKu.getName() << std::endl;
		dataKu << AyamKu.getPower() << std::endl;
		dataKu << AyamMu.getLevel();
		dataKu.close();
	}

	void InputData(){
		dataKu.open("data.dat", std::ios::in);
		dataKu >> isSaved;
		dataKu >> tempId;
		dataKu >> tempName;
		dataKu >> tempPower;
		dataKu >> currentLevel;
		dataKu.close();

		AyamKu.setId(tempId);
		AyamKu.setName(tempName);
		AyamKu.setPower(tempPower);
		AyamMu.setLevel(currentLevel);
	}

	void SelectLevel(){
		// kabupaten
		if (currentLevel == 101){
			AyamMu.setId(lv::Satu.id);
			AyamMu.setName(lv::Satu.name);
			AyamMu.setPower(lv::Satu.power);
			AyamMu.setLevel(lv::Satu.level); 
		} else if (currentLevel == 102){
			AyamMu.setId(lv::Dua.id);
			AyamMu.setName(lv::Dua.name);
			AyamMu.setPower(lv::Dua.power);
			AyamMu.setLevel(lv::Dua.level);
		} else if (currentLevel == 103){
			AyamMu.setId(lv::Tiga.id);
			AyamMu.setName(lv::Tiga.name);
			AyamMu.setPower(lv::Tiga.power);
			AyamMu.setLevel(lv::Tiga.level);
		} else if (currentLevel == 104){
			AyamMu.setId(lv::Empat.id);
			AyamMu.setName(lv::Empat.name);
			AyamMu.setPower(lv::Empat.power);
			AyamMu.setLevel(lv::Empat.level);
		} else if (currentLevel == 105){
			AyamMu.setId(lv::Lima.id);
			AyamMu.setName(lv::Lima.name);
			AyamMu.setPower(lv::Lima.power);
			AyamMu.setLevel(lv::Lima.level);
		// provinsi
		} else if (currentLevel == 106){
			AyamMu.setId(lv::Enam.id);
			AyamMu.setName(lv::Enam.name);
			AyamMu.setPower(lv::Enam.power);
			AyamMu.setLevel(lv::Enam.level);
		} else if (currentLevel == 107){
			AyamMu.setId(lv::Tujuh.id);
			AyamMu.setName(lv::Tujuh.name);
			AyamMu.setPower(lv::Tujuh.power);
			AyamMu.setLevel(lv::Tujuh.level);
		} else if (currentLevel == 108){
			AyamMu.setId(lv::Delapan.id);
			AyamMu.setName(lv::Delapan.name);
			AyamMu.setPower(lv::Delapan.power);
			AyamMu.setLevel(lv::Delapan.level);
		} else if (currentLevel == 109){
			AyamMu.setId(lv::Sembilan.id);
			AyamMu.setName(lv::Sembilan.name);
			AyamMu.setPower(lv::Sembilan.power);
			AyamMu.setLevel(lv::Sembilan.level);
		} else if (currentLevel == 110){
			AyamMu.setId(lv::Sepuluh.id);
			AyamMu.setName(lv::Sepuluh.name);
			AyamMu.setPower(lv::Sepuluh.power);
			AyamMu.setLevel(lv::Sepuluh.level);
		} else if (currentLevel == 111){
			AyamMu.setId(lv::Sebelas.id);
			AyamMu.setName(lv::Sebelas.name);
			AyamMu.setPower(lv::Sebelas.power);
			AyamMu.setLevel(lv::Sebelas.level);
		} else if (currentLevel == 112){
			AyamMu.setId(lv::Duabelas.id);
			AyamMu.setName(lv::Duabelas.name);
			AyamMu.setPower(lv::Duabelas.power);
			AyamMu.setLevel(lv::Duabelas.level);
		// nasional
		} else if (currentLevel == 113){
			AyamMu.setId(lv::Tigabelas.id);
			AyamMu.setName(lv::Tigabelas.name);
			AyamMu.setPower(lv::Tigabelas.power);
			AyamMu.setLevel(lv::Tigabelas.level);
		} else if (currentLevel == 114){
			AyamMu.setId(lv::Empatbelas.id);
			AyamMu.setName(lv::Empatbelas.name);
			AyamMu.setPower(lv::Empatbelas.power);
			AyamMu.setLevel(lv::Empatbelas.level);
		} else if (currentLevel == 115){
			AyamMu.setId(lv::Limabelas.id);
			AyamMu.setName(lv::Limabelas.name);
			AyamMu.setPower(lv::Limabelas.power);
			AyamMu.setLevel(lv::Limabelas.level);
		} else if (currentLevel == 116){
			AyamMu.setId(lv::Enambelas.id);
			AyamMu.setName(lv::Enambelas.name);
			AyamMu.setPower(lv::Enambelas.power);
			AyamMu.setLevel(lv::Enambelas.level);
		} else if (currentLevel == 117){
			AyamMu.setId(lv::Tujuhbelas.id);
			AyamMu.setName(lv::Tujuhbelas.name);
			AyamMu.setPower(lv::Tujuhbelas.power);
			AyamMu.setLevel(lv::Tujuhbelas.level);
		} else if (currentLevel == 118){
			AyamMu.setId(lv::Delapanbelas.id);
			AyamMu.setName(lv::Delapanbelas.name);
			AyamMu.setPower(lv::Delapanbelas.power);
			AyamMu.setLevel(lv::Delapanbelas.level);
		} else if (currentLevel == 119){
			AyamMu.setId(lv::Sembilanbelas.id);
			AyamMu.setName(lv::Sembilanbelas.name);
			AyamMu.setPower(lv::Sembilanbelas.power);
			AyamMu.setLevel(lv::Sembilanbelas.level);
		} else if (currentLevel == 120){
			AyamMu.setId(lv::Duapuluh.id);
			AyamMu.setName(lv::Duapuluh.name);
			AyamMu.setPower(lv::Duapuluh.power);
			AyamMu.setLevel(lv::Duapuluh.level);
		} else if (currentLevel == 121){
			AyamMu.setId(lv::Duapuluhsatu.id);
			AyamMu.setName(lv::Duapuluhsatu.name);
			AyamMu.setPower(lv::Duapuluhsatu.power);
			AyamMu.setLevel(lv::Duapuluhsatu.level);
		} else if (currentLevel == 122){
			AyamMu.setId(lv::Duapuluhdua.id);
			AyamMu.setName(lv::Duapuluhdua.name);
			AyamMu.setPower(lv::Duapuluhdua.power);
			AyamMu.setLevel(lv::Duapuluhdua.level);
		} else if (currentLevel == 123){
			AyamMu.setLevel(123);
		} else {
			std::cout << "Error: level" << std::endl;
		}
	}
}